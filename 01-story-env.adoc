## Cas d'usage: environement à la volé

by https://www.commitstrip.com/fr/

### Je dois tester mes modifications... mais plus aucun environement de libre

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/developer_man.png[title="dev",150,200,float="left"]

### bha pop un environnement temporairement

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/sysadmin.png[title="sysadmin",150,200,float="right"]

### ouai ca va me prendre des heures

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/developer_man.png[title="dev",150,200,float="left"]

### Ton infra n'est pas as code?

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/sysadmin.png[title="sysadmin",150,200,float="right"]