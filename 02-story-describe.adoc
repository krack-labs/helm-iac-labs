## Cas d'usage: un environement toujours décrit

by https://www.commitstrip.com/fr/

### Le prod est down, une VM à cramé chez OVH

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/sysadmin_angry.png[title="sysadmin",150,200,float="right"]

### Mais nous avons des backups des données

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/developer_man.png[title="dev",150,200,float="left"]

### Oui, mais pas de notre VM, des installations, de notre configuration de sécurité dans le SG

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/sysadmin.png[title="sysadmin",150,200,float="right"]

### Merde... ca va nous prendre des heures... Des jours....

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/developer_man.png[title="dev",150,200,float="left"]

### On aurait du mettre en place le Iac avant.... 

image:https://gitlab.com/krack-labs/commit-strip-lib/-/raw/main/sysadmin.png[title="sysadmin",150,200,float="right"]