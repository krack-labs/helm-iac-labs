# helm-iac-labs

## create application
```bash 
docker build --no-cache -t localhost:5000/myapp:1.0 -f ./application/Dockerfile ./application/
docker push localhost:5000/myapp:1.0
```
## deploy application

```bash 
kubectl apply -f ./deployments/mydeployment.yml
```

[http://localhost/](http://localhost/)

```bash 
kubectl delete -f ./deployments/mydeployment.yml
```

[http://localhost/](http://localhost/)

## template with helm
Check file: 
- mypackage/Chart.yaml
- mypackage/templates/deployment.yaml
- mypackage/values.yaml

```bash 
helm package deployments/mypackage
helm upgrade -i  mypackage ./mypackage-0.1.0.tgz --values ./deployments/mypackage/values.yaml
helm list
```

[http://localhost/prod/](http://localhost/prod/)

## update application
In `application/html/index.html` change color 

### Create new version
```bash 
docker build -t localhost:5000/myapp:2.0 -f ./application/Dockerfile ./application/
docker push localhost:5000/myapp:2.0
```


### Change application version 
In mypackage/Chart.yaml, change the `appVersion` with `2.0` and `version` with `0.2.0` 
```bash 
helm package deployments/mypackage
helm upgrade -i  mypackage ./mypackage-0.2.0.tgz --values ./deployments/mypackage/values.yaml
```

[http://localhost/prod/](http://localhost/prod/)

## environment on demand
Create `mypackage/values.dev.yaml` file with content : 
```
replicaCount: 1
```

```bash 
kubectl create ns sgn
helm upgrade -i  mypackage ./mypackage-0.1.0.tgz --namespace sgn --values ./deployments/mypackage/values.dev.yaml --set path=/sgn
```

[http://localhost/sgn](http://localhost/sgn)
```bash 
helm uninstall mypackage -n sgn
kubectl delete ns sgn
```


## Automatic acceptance tests
```bash 
helm test mypackage
```

Check file: 
- mypackage/templates/tests